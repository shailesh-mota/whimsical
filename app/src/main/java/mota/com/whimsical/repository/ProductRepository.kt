package mota.com.whimsical.repository

import mota.com.whimsical.db.ProductDao

class ProductRepository private constructor(private val productDao: ProductDao) {
    fun getAllProducts() = productDao.getAllProducts()

    companion object {
        // For singleton instantiation
        @Volatile
        private var instance: ProductRepository? = null

        fun getInstance(productDao: ProductDao) =
            instance ?: synchronized(this) {
                instance
                    ?: ProductRepository(productDao).also { instance = it }
            }

    }
}