package mota.com.whimsical.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import mota.com.whimsical.web.FengShuiWebService
import retrofit2.HttpException


class FengShuiRepository private constructor(private val webService: FengShuiWebService) {

    companion object {
        // For singleton instantiation
        @Volatile
        private var instance: FengShuiRepository? = null

        fun getInstance(webService: FengShuiWebService) =
            instance ?: synchronized(this) {
                instance
                    ?: FengShuiRepository(webService).also { instance = it }
            }

    }


    fun getSecretFriend(year: String, month: String, day: String, gender: String ): LiveData<String> {
        var secretFriend: MutableLiveData<String> = MutableLiveData()
        GlobalScope.launch(Dispatchers.Main) {
            val request = webService.getSecretFriend(year, month, day, gender)
            try {
                val response = request.await()
                if(response.isSuccessful) secretFriend.value = response.body()!!.result
                Log.i("Mota","Coroutine executed: " + secretFriend.value)
            } catch (e: HttpException) {
                // TODO Handle errors SEALED classes ?
                Log.i("Mota","HttpException")
            } catch (e: Throwable) {
                // TODO Handle errors
                Log.i("Mota","Other Exception" + e.toString())
            }
        }
        return secretFriend
    }
}
