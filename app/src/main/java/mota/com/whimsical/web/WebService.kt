package mota.com.whimsical.web

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FengShuiWebService {

    // e.g. Status : ok Result : snake
    @GET("findSecretFriend")
    fun getSecretFriend(@Query("year") year: String, @Query("month") month: String,  @Query("day") day: String, @Query("gender") gender: String) : Deferred<Response<FengShuiApiBaseResponse>>

    //e.g. Status : ok Result : $$$
    @GET("findBusinessCompatibility")
    fun getBusinessCompatibility(@Query("year") year: String, @Query("month") month: String,  @Query("day") day: String, @Query("year2") year2: String, @Query("month2") month2: String,  @Query("day2") day2: String) : Deferred<Response<FengShuiApiBaseResponse>>

    //e.g. Status : ok Result : **
    @GET("findLoveCompatibility")
    fun getLoveCompatibility(@Query("year") year: String, @Query("month") month: String,  @Query("day") day: String, @Query("year2") year2: String, @Query("month2") month2: String,  @Query("day2") day2: String) : Deferred<Response<FengShuiApiBaseResponse>>
}

data class FengShuiApiBaseResponse(val status: String = "blah", val result: String = "blah")