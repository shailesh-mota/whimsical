package mota.com.whimsical.web

import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import mota.com.whimsical.R
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager


fun generateHttpClient(context: Context): OkHttpClient {
    // Logging
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY

    val clientBuilder = OkHttpClient.Builder()
    clientBuilder.connectTimeout(context.resources.getInteger(R.integer.timeout_connection).toLong(), TimeUnit.MILLISECONDS)
    clientBuilder.readTimeout(context.resources.getInteger(R.integer.timeout_read).toLong(), TimeUnit.MILLISECONDS)
    // add interceptor here !
    clientBuilder.addInterceptor(logging)
    clientBuilder.addInterceptor(APIKeyInterceptor(context.resources.getString(R.string.fengshui_api_key_name),context.resources.getString(R.string.fengshui_api_key))) // TODO take care when we have more than one API key
    // Add custom trust anchors
    val cert: Pair<SSLSocketFactory, X509TrustManager> = getCustomCert(context)
    clientBuilder.sslSocketFactory(cert.first, cert.second)


    return clientBuilder.build()
}

fun getRetrofit(client: OkHttpClient, url: String) : Retrofit {
    return Retrofit.Builder()
        .baseUrl(url)
        .client(client)
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

private fun getCustomCert(context: Context) : Pair<SSLSocketFactory, X509TrustManager> {
    // Load CAs from an InputStream
    val certificateFactory = CertificateFactory.getInstance("X.509")

    // Load self-signed certificate (*.crt file)
    val inputStream =  context.getResources().openRawResource(R.raw.fengshui_cert)
    val certificate = certificateFactory.generateCertificate(inputStream)
    inputStream.close()

    // Create a KeyStore containing our trusted CAs
    val keyStoreType = KeyStore.getDefaultType()
    val keyStore = KeyStore.getInstance(keyStoreType)
    keyStore.load(null, null)
    keyStore.setCertificateEntry("ca", certificate)

    // Create a TrustManager that trusts the CAs in our KeyStore.
    val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
    val trustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm)
    trustManagerFactory.init(keyStore)

    val trustManagers = trustManagerFactory.trustManagers
    val x509TrustManager = trustManagers[0] as X509TrustManager

    // Create an SSLSocketFactory that uses our TrustManager
    val sslContext = SSLContext.getInstance("SSL")
    sslContext.init(null, arrayOf(x509TrustManager), null)
    return  Pair(sslContext.socketFactory, x509TrustManager)

}