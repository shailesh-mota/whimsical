package mota.com.whimsical.web

import okhttp3.Interceptor
import okhttp3.Response

class APIKeyInterceptor (private val apiKeyName: String, private val apiKeyValue: String): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val url = request.url().newBuilder().addQueryParameter(apiKeyName, apiKeyValue).build()
        val newRequest = request.newBuilder().url(url).build()

        return chain.proceed(newRequest)
    }
}