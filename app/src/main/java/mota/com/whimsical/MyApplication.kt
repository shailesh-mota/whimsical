package mota.com.whimsical

import android.app.Application
import mota.com.whimsical.koinmodules.appModule
import org.koin.android.ext.android.startKoin

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin(this, listOf(appModule))
    }
}