package mota.com.whimsical

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import mota.com.whimsical.viewmodels.FengShuiViewModel
import mota.com.whimsical.viewmodels.ProductViewModel
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    val productViewModel: ProductViewModel by viewModel()
    val fengShuiViewModel: FengShuiViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fengShuiViewModel.getSecretFriend("1987","11","18","0").observe(this, Observer {
            Log.i("Mota","Response achieved: " + it.toString())
        })


    }
}
