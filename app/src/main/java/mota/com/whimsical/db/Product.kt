package mota.com.whimsical.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// TODO add a field to store the distance from current user location: r = sqrt((x1-x2)ˆ2 + (y1-y2)ˆ2)
@Entity(tableName = "products")
data class Product(@PrimaryKey @ColumnInfo(name = "product_id") val id: Int, var latitude: String, var longitude: String, @ColumnInfo(name = "product_tags")var tags: MutableList<String>)