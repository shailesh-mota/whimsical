package mota.com.whimsical.db

import androidx.room.TypeConverter
import mota.com.whimsical.utils.SEPARATOR_COMMA

class Converters {
    companion object {
        @TypeConverter
        @JvmStatic
        fun mutableListToString(list: MutableList<String>): String? = list.joinToString(separator = SEPARATOR_COMMA)

        @TypeConverter
        @JvmStatic
        fun stringToMutableList(string: String?): MutableList<String>? = string?.split(SEPARATOR_COMMA)?.toMutableList()
    }
}