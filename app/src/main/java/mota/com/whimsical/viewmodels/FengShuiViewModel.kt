package mota.com.whimsical.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import mota.com.whimsical.repository.FengShuiRepository

class FengShuiViewModel(private val repository: FengShuiRepository) : ViewModel() {
    fun getSecretFriend(year: String, month: String, day: String, gender: String): LiveData<String> {
        return repository.getSecretFriend(year, month, day, gender)
    }
}