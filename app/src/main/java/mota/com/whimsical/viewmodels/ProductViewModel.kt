package mota.com.whimsical.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import mota.com.whimsical.db.Product
import mota.com.whimsical.repository.ProductRepository

class ProductViewModel(private val productRepository: ProductRepository) : ViewModel() {
    private val products: LiveData<List<Product>> = productRepository.getAllProducts()

    fun getAllProducts(): LiveData<List<Product>> = products
}