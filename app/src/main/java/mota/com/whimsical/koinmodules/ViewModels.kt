package mota.com.whimsical.koinmodules

import mota.com.whimsical.R
import mota.com.whimsical.db.AppDatabase
import mota.com.whimsical.repository.FengShuiRepository
import mota.com.whimsical.repository.ProductRepository
import mota.com.whimsical.viewmodels.FengShuiViewModel
import mota.com.whimsical.viewmodels.ProductViewModel
import mota.com.whimsical.web.FengShuiWebService
import mota.com.whimsical.web.generateHttpClient
import mota.com.whimsical.web.getRetrofit
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {
    // single instance of ProductRepository
    single { ProductRepository.getInstance(AppDatabase.getInstance(androidApplication()).productDao()) }
    viewModel { ProductViewModel(get()) }

    single { FengShuiRepository.getInstance(getRetrofit(generateHttpClient(androidApplication()), androidApplication().resources.getString(R.string.fengshui_server_url)).create(FengShuiWebService::class.java))}
    viewModel { FengShuiViewModel(get()) }
}