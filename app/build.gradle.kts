
plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
}


android {
    compileSdkVersion(Config.Android.compileSdkVersion)
    buildToolsVersion(Config.Android.buildToolsVersion)

    defaultConfig {
        applicationId = Config.Android.applicationId
        minSdkVersion(Config.Android.minSdkVersion)
        targetSdkVersion(Config.Android.targetSdkVersion)
        versionCode = Config.Android.versionCode
        versionName = Config.Android.versionName

        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
        buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Config.CompilerLibs.kotlinStdlib)
    implementation(Config.Libs.appCompat)
    implementation(Config.Libs.constraintLayout)
    implementation(Config.Libs.room)
    implementation(Config.Libs.lifecycleExtension)
    implementation(Config.Libs.koin)
    implementation(Config.Libs.retrofit) {
        exclude(module = "okhttp")
    }
    implementation(Config.Libs.retrofitGson)
    implementation(Config.Libs.retrofitCoroutineAdapter)
    implementation(Config.Libs.okhttpLogging)
    implementation(Config.Libs.okhttp)

    api(Config.Libs.coroutineCore)
    api(Config.Libs.coroutineAndroid)

    testImplementation(Config.TestLibs.roomTesting)
    testImplementation(Config.TestLibs.junit)

    androidTestImplementation(Config.TestLibs.runner)
    androidTestImplementation(Config.TestLibs.espresso)

    kapt(Config.CompilerLibs.room)
    kapt(Config.CompilerLibs.lifecycle)
}
