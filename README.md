Learnings : 
1. The ViewModel should expose states for the View, rather than just events. For example, if we need to display the name and the email address of a User, rather than creating two streams for this, we create a DisplayableUser object that encapsulates the two fields. The stream will emit every time the display name or the email changes. This way, we ensure that our View always displays the current state of the User. More here : https://medium.com/upday-devs/android-architecture-patterns-part-3-model-view-viewmodel-e7eeee76b73b
2. From the same link : We should make sure that every action of the user goes through the ViewModel and that any possible logic of the View is moved in the ViewModel.
3. 
````
val vm: UserViewModel by lazy { ViewModelProviders.of(this).get(UserViewModel::class.java) }

````

This way, the ViewModel will only be instantiated if it does not yet exist in the same scope. If it already exists, the library will return the same instance it was already using. So even if you do not use the lazy delegate, and make this call in the onCreate , you are guaranteed to receive the same ViewModel every time. It will only be created once. From : https://proandroiddev.com/view-model-creation-in-android-android-architecture-components-kotlin-ce9f6b93a46b

4. There is an unchecked cast in the below code commited with : 02d75e3d6f945f7aa7dc3a93b572960e6337b9f3
````
class ProductViewModelFactory(application: Application, private val repository: ProductRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ProductViewModel(repository) as T
    }
}
````
Following : https://proandroiddev.com/view-model-creation-in-android-android-architecture-components-kotlin-ce9f6b93a46b, mentions the same point :
It is not typesafe, the UserViewModelFactory could very well return a different ViewModel, and the project would still compile, but the app would crash during runtime because we are getting a different type than that specified at the ViewModelProviders
I made a commit to fix it in : 0e8c6f75dd44c837bb9439c19f0d055d6a39a64b

5. Another way to use Injectors is like : https://github.com/googlesamples/android-sunflower/blob/master/app/src/main/java/com/google/samples/apps/sunflower/utilities/InjectorUtils.kt, which is based on a Service Locator pattern : https://en.wikipedia.org/wiki/Service_locator_pattern, A central registery to obtain needed dependency for a class.

6. Try out (untill something better) DI with koin : 73f8950c4a0b9cde98f6fb0831f0565aa424be1d, https://github.com/InsertKoinIO/koin#ready-for-viewmodel

7. For performing background networking operations WorkManager (now stable) is a good idea  : https://proandroiddev.com/experiments-with-android-workmanager-our-new-reliable-assistant-for-deferrable-background-work-9baeb6bd7db3

8. Followed : https://antonioleiva.com/kotlin-dsl-gradle/, for using Kotlin gradle DSL in this project.

9. Followed : https://proandroiddev.com/migrating-android-app-to-gradle-kotlin-dsl-1-0-c903760275a5, for using Kotlin gradle DSL in this project.

10. Followed : https://proandroiddev.com/migrating-android-build-scripts-from-groovy-to-kotlin-dsl-f8db79dd6737, for using Kotlin gradle DSL in this project.

11. **Important** Kotlin Coroutines Concepts followed from : https://github.com/Kotlin/kotlinx.coroutines/blob/master/docs/coroutines-guide.md
12. **Important** Coroutine Dispatchers : https://github.com/Kotlin/kotlinx.coroutines/blob/master/docs/coroutine-context-and-dispatchers.md
13. **Important** Coroutine Exception Handling :https://github.com/Kotlin/kotlinx.coroutines/blob/master/docs/exception-handling.md. Can be used to aggregate network req. exception/failures.

14. Nice coroutine summary : https://proandroiddev.com/how-to-make-sense-of-kotlin-coroutines-b666c7151b93
