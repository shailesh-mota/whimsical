

private const val androidGradleVersion = "3.2.1"
private const val kotlinVersion = "1.3.11"

// dependencies
private const val roomVersion = "2.1.0-alpha03"
private const val lifecycleVersion = "2.0.0"
private const val coroutinesVersion = "1.0.1"
private const val koinViewmodelVersion = "1.0.2"
private const val retrofitVersion = "2.4.0"
private const val okhttpVersion = "3.10.0"
private const val constraintLayoutVersion = "1.1.3"
private const val appCompatVersion = "1.0.2"
private const val junitVersion = "4.12"
private const val runnerVersion = "1.1.1"
private const val espressoVersion = "3.1.1"
private const val retrofitCoroutineAdapterVersion = "0.9.2"



object Config {
    object BuildPlugins {
        val androidGradle = "com.android.tools.build:gradle:$androidGradleVersion"
        val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    }

    object Android {
        val buildToolsVersion = "28.0.3"
        val minSdkVersion = 19
        val targetSdkVersion = 28
        val compileSdkVersion = 28
        val applicationId = "mota.com.whimsical"
        val versionCode = 1
        val versionName = "1.0"
    }

    object Libs {
        val room = "androidx.room:room-runtime:$roomVersion"
        val lifecycleExtension = "androidx.lifecycle:lifecycle-extensions:$lifecycleVersion"
        val koin = "org.koin:koin-androidx-viewmodel:$koinViewmodelVersion"
        val coroutineCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion"
        val coroutineAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion"
        val retrofit = "com.squareup.retrofit2:retrofit:$retrofitVersion"
        val retrofitCoroutineAdapter = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:$retrofitCoroutineAdapterVersion"
        val retrofitGson = "com.squareup.retrofit2:converter-gson:$retrofitVersion"
        val okhttp = "com.squareup.okhttp3:okhttp:$okhttpVersion"
        val okhttpLogging = "com.squareup.okhttp3:logging-interceptor:$okhttpVersion"

        // UI
        val constraintLayout = "androidx.constraintlayout:constraintlayout:$constraintLayoutVersion"
        val appCompat = "androidx.appcompat:appcompat:$appCompatVersion"

    }

    object TestLibs {
        val roomTesting = "androidx.room:room-testing:$roomVersion"
        val junit = "junit:junit:$junitVersion"
        val runner = "androidx.test:runner:$runnerVersion"
        val espresso = "androidx.test.espresso:espresso-core:$espressoVersion"
    }

    object CompilerLibs {
        val room = "androidx.room:room-compiler:$roomVersion"
        val lifecycle = "androidx.lifecycle:lifecycle-compiler:$lifecycleVersion"
        val kotlinStdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion"
    }
}